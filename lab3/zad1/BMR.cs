﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zad1
{
	public partial class BMR : Form
	{
		public BMR()
		{
			InitializeComponent();
		}

		private void label1_Click(object sender, EventArgs e)
		{

		}

		private void label2_Click(object sender, EventArgs e)
		{

		}

		private void label3_Click(object sender, EventArgs e)
		{

		}

		private void textBox4_TextChanged(object sender, EventArgs e)
		{

		}

		private void radioButton1_CheckedChanged(object sender, EventArgs e)
		{

		}

		private void btnReset_Click(object sender, EventArgs e)
		{
			txtWaga.Text = "";
			txtWzrost.Text = "";
			txtWiek.Text = "";
			txtWynik.Text = "";

			radMez.Checked = false;
			radKob.Checked = false;


		}

		private void btnOblicz_Click(object sender, EventArgs e)
		{
			double waga = Convert.ToDouble(txtWaga.Text);
			double wzrost = Convert.ToDouble(txtWzrost.Text);
			double wiek = Convert.ToDouble(txtWiek.Text);
			double wynik = 0.0;

			if (radMez.Checked)
			{
				wynik = (9.99 * waga) + (6.25 * wzrost) - (4.92 * wiek) + 5.0;

			}else if (radKob.Checked)
			{
				wynik = (9.99 * waga) + (6.25 * wzrost) - (4.92 * wiek) - 161.0;
			}
			txtWynik.Text = wynik.ToString("0.0");
		}

		private void label3_Click_1(object sender, EventArgs e)
		{

		}
	}
}
