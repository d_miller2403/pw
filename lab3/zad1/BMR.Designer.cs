﻿namespace zad1
{
	partial class BMR
	{
		/// <summary>
		/// Wymagana zmienna projektanta.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Wyczyść wszystkie używane zasoby.
		/// </summary>
		/// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Kod generowany przez Projektanta formularzy systemu Windows

		/// <summary>
		/// Metoda wymagana do obsługi projektanta — nie należy modyfikować
		/// jej zawartości w edytorze kodu.
		/// </summary>
		private void InitializeComponent()
		{
            this.lblWaga = new System.Windows.Forms.Label();
            this.lblWzrost = new System.Windows.Forms.Label();
            this.lblWiek = new System.Windows.Forms.Label();
            this.lblWynik = new System.Windows.Forms.Label();
            this.txtWaga = new System.Windows.Forms.TextBox();
            this.txtWzrost = new System.Windows.Forms.TextBox();
            this.txtWiek = new System.Windows.Forms.TextBox();
            this.txtWynik = new System.Windows.Forms.TextBox();
            this.radMez = new System.Windows.Forms.RadioButton();
            this.radKob = new System.Windows.Forms.RadioButton();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnOblicz = new System.Windows.Forms.Button();
            this.lblKg = new System.Windows.Forms.Label();
            this.lblCm = new System.Windows.Forms.Label();
            this.lblLat = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblWaga
            // 
            this.lblWaga.AutoSize = true;
            this.lblWaga.Location = new System.Drawing.Point(62, 60);
            this.lblWaga.Name = "lblWaga";
            this.lblWaga.Size = new System.Drawing.Size(36, 13);
            this.lblWaga.TabIndex = 0;
            this.lblWaga.Text = "Waga";
            this.lblWaga.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblWzrost
            // 
            this.lblWzrost.AutoSize = true;
            this.lblWzrost.Location = new System.Drawing.Point(58, 99);
            this.lblWzrost.Name = "lblWzrost";
            this.lblWzrost.Size = new System.Drawing.Size(40, 13);
            this.lblWzrost.TabIndex = 1;
            this.lblWzrost.Text = "Wzrost";
            this.lblWzrost.Click += new System.EventHandler(this.label2_Click);
            // 
            // lblWiek
            // 
            this.lblWiek.AutoSize = true;
            this.lblWiek.Location = new System.Drawing.Point(66, 140);
            this.lblWiek.Name = "lblWiek";
            this.lblWiek.Size = new System.Drawing.Size(32, 13);
            this.lblWiek.TabIndex = 2;
            this.lblWiek.Text = "Wiek";
            this.lblWiek.Click += new System.EventHandler(this.label3_Click);
            // 
            // lblWynik
            // 
            this.lblWynik.AutoSize = true;
            this.lblWynik.Location = new System.Drawing.Point(49, 243);
            this.lblWynik.Name = "lblWynik";
            this.lblWynik.Size = new System.Drawing.Size(40, 13);
            this.lblWynik.TabIndex = 3;
            this.lblWynik.Text = "Wynik:";
            // 
            // txtWaga
            // 
            this.txtWaga.Location = new System.Drawing.Point(98, 57);
            this.txtWaga.Name = "txtWaga";
            this.txtWaga.Size = new System.Drawing.Size(80, 20);
            this.txtWaga.TabIndex = 4;
            // 
            // txtWzrost
            // 
            this.txtWzrost.Location = new System.Drawing.Point(98, 96);
            this.txtWzrost.Name = "txtWzrost";
            this.txtWzrost.Size = new System.Drawing.Size(80, 20);
            this.txtWzrost.TabIndex = 5;
            // 
            // txtWiek
            // 
            this.txtWiek.Location = new System.Drawing.Point(98, 137);
            this.txtWiek.Name = "txtWiek";
            this.txtWiek.Size = new System.Drawing.Size(80, 20);
            this.txtWiek.TabIndex = 6;
            // 
            // txtWynik
            // 
            this.txtWynik.BackColor = System.Drawing.SystemColors.ControlLight;
            this.txtWynik.Location = new System.Drawing.Point(52, 259);
            this.txtWynik.Multiline = true;
            this.txtWynik.Name = "txtWynik";
            this.txtWynik.ReadOnly = true;
            this.txtWynik.Size = new System.Drawing.Size(126, 50);
            this.txtWynik.TabIndex = 7;
            this.txtWynik.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // radMez
            // 
            this.radMez.AutoSize = true;
            this.radMez.Location = new System.Drawing.Point(61, 193);
            this.radMez.Name = "radMez";
            this.radMez.Size = new System.Drawing.Size(78, 17);
            this.radMez.TabIndex = 8;
            this.radMez.TabStop = true;
            this.radMez.Text = "Mężczyzna";
            this.radMez.UseVisualStyleBackColor = true;
            this.radMez.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radKob
            // 
            this.radKob.AutoSize = true;
            this.radKob.Location = new System.Drawing.Point(150, 193);
            this.radKob.Name = "radKob";
            this.radKob.Size = new System.Drawing.Size(61, 17);
            this.radKob.TabIndex = 9;
            this.radKob.TabStop = true;
            this.radKob.Text = "Kobieta";
            this.radKob.UseVisualStyleBackColor = true;
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(52, 315);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(70, 25);
            this.btnReset.TabIndex = 10;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnOblicz
            // 
            this.btnOblicz.Location = new System.Drawing.Point(184, 259);
            this.btnOblicz.Name = "btnOblicz";
            this.btnOblicz.Size = new System.Drawing.Size(59, 50);
            this.btnOblicz.TabIndex = 11;
            this.btnOblicz.Text = "Oblicz";
            this.btnOblicz.UseVisualStyleBackColor = true;
            this.btnOblicz.Click += new System.EventHandler(this.btnOblicz_Click);
            // 
            // lblKg
            // 
            this.lblKg.AutoSize = true;
            this.lblKg.Location = new System.Drawing.Point(184, 60);
            this.lblKg.Name = "lblKg";
            this.lblKg.Size = new System.Drawing.Size(19, 13);
            this.lblKg.TabIndex = 12;
            this.lblKg.Text = "kg";
            // 
            // lblCm
            // 
            this.lblCm.AutoSize = true;
            this.lblCm.Location = new System.Drawing.Point(184, 99);
            this.lblCm.Name = "lblCm";
            this.lblCm.Size = new System.Drawing.Size(21, 13);
            this.lblCm.TabIndex = 13;
            this.lblCm.Text = "cm";
            // 
            // lblLat
            // 
            this.lblLat.AutoSize = true;
            this.lblLat.Location = new System.Drawing.Point(184, 140);
            this.lblLat.Name = "lblLat";
            this.lblLat.Size = new System.Drawing.Size(18, 13);
            this.lblLat.TabIndex = 14;
            this.lblLat.Text = "lat";
            this.lblLat.Click += new System.EventHandler(this.label3_Click_1);
            // 
            // BMR
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(281, 371);
            this.Controls.Add(this.lblLat);
            this.Controls.Add(this.lblCm);
            this.Controls.Add(this.lblKg);
            this.Controls.Add(this.btnOblicz);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.radKob);
            this.Controls.Add(this.radMez);
            this.Controls.Add(this.txtWynik);
            this.Controls.Add(this.txtWiek);
            this.Controls.Add(this.txtWzrost);
            this.Controls.Add(this.txtWaga);
            this.Controls.Add(this.lblWynik);
            this.Controls.Add(this.lblWiek);
            this.Controls.Add(this.lblWzrost);
            this.Controls.Add(this.lblWaga);
            this.Name = "BMR";
            this.Text = "BMR";
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblWaga;
		private System.Windows.Forms.Label lblWzrost;
		private System.Windows.Forms.Label lblWiek;
		private System.Windows.Forms.Label lblWynik;
		private System.Windows.Forms.TextBox txtWaga;
		private System.Windows.Forms.TextBox txtWzrost;
		private System.Windows.Forms.TextBox txtWiek;
		private System.Windows.Forms.TextBox txtWynik;
		private System.Windows.Forms.RadioButton radMez;
		private System.Windows.Forms.RadioButton radKob;
		private System.Windows.Forms.Button btnReset;
		private System.Windows.Forms.Button btnOblicz;
        private System.Windows.Forms.Label lblKg;
        private System.Windows.Forms.Label lblCm;
        private System.Windows.Forms.Label lblLat;
    }
}

