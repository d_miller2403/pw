﻿namespace zad2
{
    partial class PESEL
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPesel = new System.Windows.Forms.Label();
            this.txtPesel = new System.Windows.Forms.TextBox();
            this.lblSprawdz = new System.Windows.Forms.Button();
            this.txtWynik = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblPesel
            // 
            this.lblPesel.AutoSize = true;
            this.lblPesel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblPesel.Location = new System.Drawing.Point(31, 62);
            this.lblPesel.Name = "lblPesel";
            this.lblPesel.Size = new System.Drawing.Size(60, 18);
            this.lblPesel.TabIndex = 0;
            this.lblPesel.Text = "PESEL:";
            // 
            // txtPesel
            // 
            this.txtPesel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtPesel.Location = new System.Drawing.Point(88, 56);
            this.txtPesel.Multiline = true;
            this.txtPesel.Name = "txtPesel";
            this.txtPesel.Size = new System.Drawing.Size(152, 30);
            this.txtPesel.TabIndex = 1;
            // 
            // lblSprawdz
            // 
            this.lblSprawdz.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblSprawdz.Location = new System.Drawing.Point(137, 216);
            this.lblSprawdz.Name = "lblSprawdz";
            this.lblSprawdz.Size = new System.Drawing.Size(103, 47);
            this.lblSprawdz.TabIndex = 2;
            this.lblSprawdz.Text = "SPRAWDŹ!";
            this.lblSprawdz.UseVisualStyleBackColor = true;
            this.lblSprawdz.Click += new System.EventHandler(this.lblSprawdz_Click);
            // 
            // txtWynik
            // 
            this.txtWynik.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtWynik.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtWynik.Location = new System.Drawing.Point(34, 92);
            this.txtWynik.Multiline = true;
            this.txtWynik.Name = "txtWynik";
            this.txtWynik.ReadOnly = true;
            this.txtWynik.Size = new System.Drawing.Size(206, 47);
            this.txtWynik.TabIndex = 3;
            // 
            // PESEL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(291, 306);
            this.Controls.Add(this.txtWynik);
            this.Controls.Add(this.lblSprawdz);
            this.Controls.Add(this.txtPesel);
            this.Controls.Add(this.lblPesel);
            this.Name = "PESEL";
            this.Text = "PESEL";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPesel;
        private System.Windows.Forms.TextBox txtPesel;
        private System.Windows.Forms.Button lblSprawdz;
        private System.Windows.Forms.TextBox txtWynik;
    }
}

