﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace zad2
{
    public partial class PESEL : Form
    {
        public PESEL()
        {
            InitializeComponent();
        }

        private void lblSprawdz_Click(object sender, EventArgs e)
        {

            String P = txtPesel.Text;
            int d = P.Length;
            int[] tab = new int[d];
            for (int i = 0; i < d; i++)
            {
                tab[i] = Convert.ToInt32(Convert.ToString(P[i]));
            }

            int wynik = (1 * tab[0]) + (3 * tab[1]) + (7 * tab[2]) + (9 * tab[3]) + (1 * tab[4]) + (3 * tab[5])
                        + (7 * tab[6]) + (9 * tab[7]) + (1 * tab[8]) + (3 * tab[9]);

            int cyfra = wynik % 10;
            int wynik2 = 10 - cyfra;

            if (wynik2 == tab[10])
            {
                txtWynik.Text = "Poprawny PESEL!";
            }
            else
            {
                txtWynik.Text = "Błędny PESEL!";
            }

            Debug.WriteLine(d);
            Debug.WriteLine(P[10]);
        }
    }
}
