﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kostka
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void lbl_Click(object sender, EventArgs e)
        {

        }

        private void lblNumer_Click(object sender, EventArgs e)
        {

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            Random rand = new Random();
            if(e.KeyCode == Keys.R)
            {
                lblNumer.Text = Convert.ToString(rand.Next(1, 7));
            }
        }

        private void lblNumer_DoubleClick(object sender, EventArgs e)
        {
            Clipboard.SetText(lblNumer.Text);
        }
    }
}
