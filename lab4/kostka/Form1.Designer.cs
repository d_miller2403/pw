﻿namespace kostka
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNumer = new System.Windows.Forms.Label();
            this.lblLosuj = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblNumer
            // 
            this.lblNumer.AutoSize = true;
            this.lblNumer.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNumer.Location = new System.Drawing.Point(117, 70);
            this.lblNumer.Name = "lblNumer";
            this.lblNumer.Size = new System.Drawing.Size(99, 108);
            this.lblNumer.TabIndex = 0;
            this.lblNumer.Text = "0";
            this.lblNumer.Click += new System.EventHandler(this.lblNumer_Click);
            this.lblNumer.DoubleClick += new System.EventHandler(this.lblNumer_DoubleClick);
            // 
            // lblLosuj
            // 
            this.lblLosuj.AutoSize = true;
            this.lblLosuj.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblLosuj.Location = new System.Drawing.Point(12, 198);
            this.lblLosuj.Name = "lblLosuj";
            this.lblLosuj.Size = new System.Drawing.Size(306, 18);
            this.lblLosuj.TabIndex = 1;
            this.lblLosuj.Text = "WCIŚNIJ KLAWISZ \"R\" ABY LOSOWAĆ";
            this.lblLosuj.Click += new System.EventHandler(this.lbl_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(330, 274);
            this.Controls.Add(this.lblLosuj);
            this.Controls.Add(this.lblNumer);
            this.Name = "Form1";
            this.Text = "Kostka do gry";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNumer;
        private System.Windows.Forms.Label lblLosuj;
    }
}

