﻿namespace login
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblWitaj = new System.Windows.Forms.Label();
            this.lblNazwa = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblWitaj
            // 
            this.lblWitaj.AutoSize = true;
            this.lblWitaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblWitaj.Location = new System.Drawing.Point(46, 95);
            this.lblWitaj.Name = "lblWitaj";
            this.lblWitaj.Size = new System.Drawing.Size(221, 48);
            this.lblWitaj.TabIndex = 0;
            this.lblWitaj.Text = "Witaj! \r\nZalogowałeś się jako: ";
            // 
            // lblNazwa
            // 
            this.lblNazwa.AutoSize = true;
            this.lblNazwa.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNazwa.ForeColor = System.Drawing.Color.Blue;
            this.lblNazwa.Location = new System.Drawing.Point(257, 119);
            this.lblNazwa.Name = "lblNazwa";
            this.lblNazwa.Size = new System.Drawing.Size(76, 24);
            this.lblNazwa.TabIndex = 1;
            this.lblNazwa.Text = "######";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(388, 253);
            this.Controls.Add(this.lblNazwa);
            this.Controls.Add(this.lblWitaj);
            this.Name = "Form2";
            this.Text = "Witaj!";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblWitaj;
        private System.Windows.Forms.Label lblNazwa;
    }
}