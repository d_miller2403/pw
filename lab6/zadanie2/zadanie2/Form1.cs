﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace zadanie2
{
    public partial class Form1 : Form
    {
        BackgroundWorker Worker;

        public Form1()
        {
            InitializeComponent();
            Worker = new BackgroundWorker();
            Worker.DoWork += new DoWorkEventHandler(Worker_DoWork);
            Worker.ProgressChanged += new ProgressChangedEventHandler(Worker_ProgressChanged);
            Worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Worker_RunWorkerCompleted);
            Worker.WorkerReportsProgress = true;
            Worker.WorkerSupportsCancellation = true;
        }


        int liczbaPierwsza()
        {
            Random rand = new Random();
            int liczba, licznik;
            while (true) { 
                licznik = 0;
                liczba = rand.Next(1, 1001);
                if (liczba == 1) continue;
                for (int j = 2; j <= (liczba / 2); j++)
                {
                    if (liczba % j == 0)
                    {
                        licznik++;
                        break;
                    }
                }
                if (licznik == 0) break;
            };

            return liczba;
        }

        String pokazLiczby(int[] tab)
        {
            string liczby = "Wylosowane liczby pierwsze:\n";
            foreach(int liczba in tab)
            {
                liczby += Convert.ToString(liczba)+"  ";
            }
            return liczby;
        }

        void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            int[] tab = new int[100];
            for (int i = 0; i < 100; i++)
            {

                tab[i] = liczbaPierwsza();
                Thread.Sleep(100);
                Worker.ReportProgress(i);
                
                if (Worker.CancellationPending)
                {
                    e.Cancel = true;
                    Worker.ReportProgress(0);
                    return;
                }

            }
            Worker.ReportProgress(100);
            MessageBox.Show(pokazLiczby(tab));
        }

        void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
            label1.Text = "Postęp...." + progressBar1.Value.ToString() + "%";
        }

        void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                label1.Text = "Proces zatrzymany.";
            }
            else if (e.Error != null)
            {
                label1.Text = "Wystąpił błąd.";
            }
            else
            {
                label1.Text = "Proces ukończony....";
            }
            btnStart.Enabled = true;
            btnStop.Enabled = false;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            btnStart.Enabled = false;
            btnStop.Enabled = true;
            Worker.RunWorkerAsync();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            if (Worker.IsBusy)
            {
            Worker.CancelAsync();
            }
        }
    }
}
